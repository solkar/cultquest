﻿using UnityEngine;
using UnityEditor;

public class CreateBlockCombination
{
	[MenuItem("Assets/Create/Block Combination")]
	public static void CreateAsset()
	{
		CustomAssetUtility.CreateAsset<BlockCombination>();
	}
}
