﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System;

public interface IPlayerListener
{
    void OnDeath();
    void OnExit();
}

public class Player : MonoBehaviour
{
    [Header( "Physics" )]
	// movement config
	public float gravity       = -25f;
	public float runSpeed      = 8f;
	public float groundDamping = 20f; // how fast do we change direction? higher means faster
	public float inAirDamping  = 5f;
	public float jumpHeight    = 3f;

    [Header( "FX" )]
    [SerializeField] AudioClip  _shapeShiftFx;
    [SerializeField] AudioClip  deathClip;
    [SerializeField] ParticleSystem _shapeShiftParticles;
    [SerializeField] ParticleSystem _deathParticles;

    [Header( "Other" )]
    [SerializeField] public Gate.Symbol           _playerSymbol;
    [SerializeField] SwitchSprite _headSwitch;

	private float                 normalizedHorizontalSpeed = 0;
	private CharacterController2D _controller;
	private Animator              _animator;
	private RaycastHit2D          _lastControllerColliderHit;
	private Vector3               _velocity;
    private bool                  holdingHead = false;
    private int                   facingDirection = 1;
    private bool                  _dead;
    private IPlayerListener       _listener;

    private PathMover _beginAnimation;

    public void RegisterListener( IPlayerListener listener )
    {
        if( _listener != null )
        {
            Debug.Log( "Listener already registered" );
        }

        _listener = listener;
    }

	void Awake()
	{
		_animator = GetComponent<Animator>();
		_controller = GetComponent<CharacterController2D>();

		// listen to some events for illustration purposes
		_controller.onControllerCollidedEvent += onControllerCollider;
		_controller.onTriggerEnterEvent       += onTriggerEnterEvent;
		_controller.onTriggerExitEvent        += onTriggerExitEvent;

        _beginAnimation = GetComponent<PathMover>();
	}


    void Start()
    {
        _listener = Game.instance;

        if( _listener == null )
            Debug.LogWarning( "[Player][Start] No listener registered" );
    }

	#region Event Listeners

	void onControllerCollider( RaycastHit2D hit )
	{
		// bail out on plain old ground hits cause they arent very interesting
		if( hit.normal.y == 1f )
			return;

		// logs any collider hits if uncommented. it gets noisy so it is commented out for the demo
        //Debug.Log( "flags: " + _controller.collisionState + ", hit.normal: " + hit.normal );
	}


	void onTriggerEnterEvent( Collider2D col )
	{
        //Debug.Log( "onTriggerEnterEvent: " + col.gameObject.name );

        if( _dead )
            return;

        if( col.CompareTag( "Gate" ) )
        {
            var gate = col.GetComponent<Gate>();

            HandleGateCollision( gate );
        }
	}


	void onTriggerExitEvent( Collider2D col )
	{
		//Debug.Log( "onTriggerExitEvent: " + col.gameObject.name );
	}

    void OnShootEvent()
    {
        holdingHead = false;
    }

	#endregion


	// the Update loop contains a very simple example of moving the character around and controlling the animation
	void Update()
	{
        if( _dead )
            return;

		if( _controller.isGrounded )
			_velocity.y = 0;

        normalizedHorizontalSpeed = 0;



		// apply horizontal speed smoothing it. dont really do this with Lerp. Use SmoothDamp or something that provides more control
		var smoothedMovementFactor = _controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
		_velocity.x = Mathf.Lerp( _velocity.x, normalizedHorizontalSpeed * runSpeed, Time.deltaTime * smoothedMovementFactor );

		// apply gravity before moving
		_velocity.y += gravity * Time.deltaTime;

		// if holding down bump up our movement amount and turn off one way platform detection for a frame.
		// this lets uf jump down through one way platforms
		if( _controller.isGrounded && Input.GetKey( KeyCode.DownArrow ) )
		{
			_velocity.y *= 3f;
			_controller.ignoreOneWayPlatformsThisFrame = true;
		}

		//_controller.move( _velocity * Time.deltaTime );

		// grab our current _velocity to use as a base for all calculations
		_velocity = _controller.velocity;

        UpdateAnimator();

        //gun.gameObject.SetActive( holdingHead );
        //gun.normalizedHorizontalSpeed = facingDirection;

	}

    void UpdateAnimator()
    {
        //_animator.SetBool( "Running" , true );
    }

    void OnDeath()
    {
        if( _dead )
            return;

        _dead = true;

        GetComponent<SpriteRenderer>().enabled = false;

        _deathParticles.Play( true );
        SoundKit.instance.playSound( deathClip );

        if( _listener != null )
            _listener.OnDeath();
        else
            Debug.LogWarning( "No OnDeath listerner" );
    }

    public void Action()
    {

        SoundKit.instance.playSound( _shapeShiftFx );

        _playerSymbol = Gate.GetNextSymbol( _playerSymbol );

        float blend = 0;

        if( _playerSymbol == Gate.Symbol.Circle ) 
        {
           blend = 0.5f; 
        }
        else if( _playerSymbol == Gate.Symbol.Square ) 
        {
            blend = 1.0f;
        }

        _animator.SetFloat( "symbol" , blend );

        _shapeShiftParticles.Play( true );
    }

    void HandleGateCollision( Gate g )
    {
        Debug.Log( "Player:" + _playerSymbol );
        Debug.Log( "Gate:" + g.GetSymbol() );
            
       if ( _playerSymbol != g.GetSymbol() )
       {
            OnDeath();
       }
       else
       {
           //TODO: disable gate
       }
    }

    public void Initialize()
    {
        _dead = false;
        _playerSymbol = Gate.Symbol.Triangle;

        _animator.SetFloat( "symbol" , (float) ( _playerSymbol - 1 )/ 3.0f );

    }

    public void BeginAnimation()
    {
        GetComponent<SpriteRenderer>().enabled = true;
        _beginAnimation.Trigger();
    }
}
