﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TransitionManager : Singleton<TransitionManager>
{
    public enum EEasingType
    {
        Linear,
        QuadOut
    }

    [SerializeField] 
    public RawImage _overlay;
    [SerializeField] 
    public float _duration = 3.0f;
    [SerializeField]
    public EEasingType _easingType = EEasingType.Linear;

    private TransitionManager() {}

    void Start()
    {
        StartCoroutine( FadeOut() );
    }

    void OnLevelWasLoaded( int level )
    {
        StartCoroutine( FadeOut() );
    }

    IEnumerator FadeOut()
    {
        float timer = 0f;
        Color ogColor = _overlay.color;

        while( timer <= _duration )
        {
            timer += Time.deltaTime;

            switch(_easingType)
            {
                case EEasingType.Linear:
                    {
                        _overlay.color = Color.Lerp(ogColor, Color.clear, timer / _duration);
                    }
                    break;

                case EEasingType.QuadOut:
                    {
                        float t = (timer / _duration - 1.0f);
                        float currentAlpha = 1.0f -(t * t);
                        _overlay.color = Color.Lerp(ogColor, Color.clear, currentAlpha);
                    }
                    break;
            }

            yield return null;
        }

    }

    IEnumerator FadeIn()
    {
        float timer = 0f;
        Color ogColor = _overlay.color;

        while( timer <= _duration )
        {
            timer += Time.deltaTime;

            switch(_easingType)
            {
                case EEasingType.Linear:
                    {
                        _overlay.color = Color.Lerp(ogColor, Color.black, timer / _duration);
                    }
                    break;

                case EEasingType.QuadOut:
                    {
                        float t = (timer / _duration - 1.0f);
                        float currentAlpha = 1.0f -(t * t);
                        _overlay.color = Color.Lerp(ogColor, Color.black, currentAlpha);
                    }
                    break;
            }

            yield return null;
        }

    }

    public void TriggerFadeIn()
    {
        StartCoroutine( FadeIn() );
    }

    public void TriggerFadeOut()
    {
        StartCoroutine( FadeOut() );
    }
}
