﻿using UnityEngine;
using System;
using System.Collections;

public class Gate : MonoBehaviour 
{
    public enum Symbol
    {
        None,
        Triangle,
        Circle,
        Square,
    }

    [SerializeField] SwitchSprite _switch;
    [SerializeField] SwitchSprite _inside;
    [SerializeField] LayerMask _playerLayer;

    private Symbol _symbol;

    public void SetSymbol( Symbol newSymbol )
    {
        _symbol = newSymbol;
        _switch.SetSpriteWithIndex( (int) _symbol );
    }
    
    public Symbol GetSymbol()
    {
        return _symbol;
    }

    public static Symbol GetNextSymbol( Symbol origin )
    {
        switch( origin )
        {
            default:
            case Symbol.Triangle:
                return Symbol.Circle;
                break;

            case Symbol.Circle:
                return Symbol.Square;
                break;

            case Symbol.Square:
                return Symbol.Triangle;
                break;

        }
    }

    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast( transform.position , -transform.right , Mathf.Infinity, _playerLayer );
        if( hit )
        {
          if( hit.collider.gameObject.GetComponent<Player>()._playerSymbol == _symbol )  
          {
              _inside.SetSpriteWithIndex( 1 );
              Debug.DrawRay( transform.position , hit.transform.position , Color.green );
          }
          else
          {
              _inside.SetSpriteWithIndex( 0 );
              Debug.DrawRay( transform.position , 100 * -transform.right, Color.red );
          }

        }
    }


}
