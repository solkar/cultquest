﻿using UnityEngine;
using System;
using System.Collections;
using Klak.Motion;

public class GateBlock : MonoBehaviour 
{
    [SerializeField] Transform _initPoint;

    Gate[] _gateArray;

    private ConstantMotion _constantMotion;

    void Awake()
    {
       _constantMotion = GetComponent<ConstantMotion>();

       _gateArray = transform.GetComponentsInChildren<Gate>();

    }

    void Start()
    {
       for( int i = 0; i < _gateArray.Length; i++ ) 
       {
           var gate = _gateArray[ i ];
           gate.SetSymbol( Gate.Symbol.Square );
       }
    }

    public void Run()
    {
        _constantMotion.enabled = true;
    }

    public void Stop()
    {
        _constantMotion.enabled = false;
    }

    public void SetOnInitPoint()
    {
        transform.position = _initPoint.position;
    }

    public void LoadCombination( BlockCombination b )
    {
        for( int i = 0; i < b.symbolArray.Length; i++ )
        {
            if( b.symbolArray[ i ] == Gate.Symbol.None )
            {
                _gateArray[ i ].gameObject.SetActive( false );
            }
            else
            {
            _gateArray[ i ].SetSymbol(  b.symbolArray[ i ] );
            }
        }
    }
}
