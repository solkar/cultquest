﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlockConsumer : MonoBehaviour 
{
    private BlockCombination[] _blockArray;
    private Queue<BlockCombination> _blockQueue;

    void Awake()
    {
        // load all assets 
        _blockArray = Resources.LoadAll<BlockCombination>( "" ); 

        RepopulateQue();
    }

    public BlockCombination ConsumeBlock()
    {
        Debug.Log( "Block que: " + _blockQueue.Count );

        var block = _blockQueue.Dequeue();

        if( _blockQueue.Count == 0 )
            RepopulateQue();

        return block;
    }

    void RepopulateQue()
    {
        var list = new List<BlockCombination>( _blockArray );
        list.Shuffle();
        _blockQueue = new Queue<BlockCombination>( list.ToArray() );
    }
}
