﻿using UnityEngine;
using System.Collections;
using UrMotion;

public class Pulsating : MonoBehaviour 
{
    [SerializeField] float _scale;
    [SerializeField] float _amp;

	void Start () 
    {
        gameObject.MotionS().AccelByRatio(Vector2.one * 0.4f, _scale ).Sin(Vector2.one * 0.5f, _amp );
	}
	
}
