﻿using UnityEngine;
using System.Collections;
using UrMotion;

public class UpAndDown : MonoBehaviour 
{

    [SerializeField] float _freq;
    [SerializeField] float _amp;

	void Start () 
    {

        gameObject.MotionY().Sin( _freq, _amp);
	}
	
}
