﻿using UnityEngine;
using System.Collections;

public class BlockCombination : ScriptableObject
{
    public enum Difficulty
    {
        Easy,
        Medium,
        Hard,
    }

    [SerializeField] public Gate.Symbol[] symbolArray;
    [SerializeField] public Difficulty _difficulty;

}
