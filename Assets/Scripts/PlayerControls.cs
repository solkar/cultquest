﻿using UnityEngine;
using System.Collections;

[RequireComponent( typeof( Player ) )]
public class PlayerControls : MonoBehaviour 
{
    [SerializeField] float _keyTimeout = 0.25f;

    private Player _player;
    private float _timer;

    void Awake()
    {
        _player = GetComponent<Player>();

        _timer = _keyTimeout;
    }

    void Update()
    {

        if( Input.GetKeyDown( KeyCode.Space ) 
                && _timer <= 0 )
        {
            _player.Action();
        }
        else if( Input.GetKeyUp( KeyCode.Space ) )
        {

            _timer = _keyTimeout;
        }
        else
        {
            _timer -= Time.deltaTime;
        }
    }
}
