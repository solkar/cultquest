﻿//
//
// Game  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using System.Collections;

[RequireComponent ( typeof ( BlockConsumer ) )]
public class Game : MonoBehaviour , IPlayerListener
{

    [Header( "Sound Fx" )]
    [SerializeField] AudioClip _bingClip;
    [SerializeField] AudioClip  _stepClip;

    [HeaderAttribute( "HUD" )]
    [SerializeField] GameObject _beginCanvas;
    [SerializeField] GameObject _gameoverCanvas;

    [HeaderAttribute( "GameObjects" )]
    [SerializeField] GateBlock _gateBlock;
    [SerializeField] OffsetScroller _background;
    [SerializeField] float _backgroundSpeed = 0.5f;
    
    [HeaderAttribute( "Data" )]
    [SerializeField] BlockCombination _testCombination;

    [SerializeField] bool reloadLevel = false;

    [HideInInspector] public static Game instance = null;

    private Player         player;
    private GameObject     menu;
    private bool           _loadingLevel = false;
    private PlayerControls _playerControls;
    private bool           _isPlayerAlive;
    private BlockConsumer _blockConsumer;

    
    const float kReloadDelay = 1.5f;
    const int kLastLevel = 4;

    void Awake()
    {
        Debug.Log( "[LevelManager] Awake" );
            
        if( instance == null )
        {
            instance = this;
        }
        else if( instance != this )
        {
            Destroy( gameObject );
        }

        DontDestroyOnLoad( gameObject );

        menu = GameObject.FindWithTag( "GameOver" );

        _loadingLevel = false;

        var playerObject = GameObject.FindWithTag( "Player" );
        if( playerObject == null )
        {
            Debug.LogError( "Cant find player" );
            return;
        }
        player = playerObject.GetComponent<Player>();
        _playerControls = player.GetComponent<PlayerControls>();

        _blockConsumer = GetComponent<BlockConsumer>();
    }

    void Start()
    {
        _beginCanvas.SetActive( false );
        _gameoverCanvas.SetActive( false );

        StartCoroutine( GameLoop() );
    }

    IEnumerator GameLoop()
    {
        yield return GameInit();

        while( true )
        {
            yield return BeginGame();
            yield return GameRunning();
            yield return GameOver();
        }
    }

#region State Machine


    IEnumerator GameInit()
    {
        Debug.Log( "Init begins" );

        // Setup hud
        _beginCanvas.SetActive( true );

        // Disable player input
        _playerControls.enabled = false;

        _gateBlock.Stop();

        // stop background scroll
        _background.scrollSpeedX = 0f;
        _background.enabled = false;
        _background.ResetOffset();
        
        yield return WaitForPlayerInput();
        SoundKit.instance.playSound( _bingClip );

        TransitionManager.Instance.TriggerFadeIn();
        float delayToFadeStartmenu = 3.0f;
        yield return new WaitForSeconds( delayToFadeStartmenu );
        _beginCanvas.SetActive( false );

        TransitionManager.Instance.TriggerFadeOut();
        yield return new WaitForSeconds( delayToFadeStartmenu );
        
        Debug.Log( "Init finished" );
    }

    IEnumerator BeginGame()
    {
        Debug.Log( "Begin game: init" );
        
        player.BeginAnimation();

        _background.scrollSpeedX = 0f;
        _background.enabled = false;
        _background.ResetOffset();

        _gateBlock.SetOnInitPoint();

        var s = SoundKit.instance.playSoundLooped( _stepClip );
        
        float beginAnimationDuration = 3.0f;
        yield return new WaitForSeconds( beginAnimationDuration );

        _background.scrollSpeedX = _backgroundSpeed;
        _background.enabled = true;
        s.stop();

        Debug.Log( "Begin game: end" );
    }

    IEnumerator GameRunning()
    {
        Debug.Log( "Running begins" );
        
        // Enable player input
        _playerControls.enabled = true;

        player.Initialize();

        _gateBlock.SetOnInitPoint();
        _gateBlock.Run();

        _gateBlock.LoadCombination( _blockConsumer.ConsumeBlock() );
        _isPlayerAlive = true;

        while( _isPlayerAlive )
        {
            yield return null;
        }
        Debug.Log( "Running ends" );
    }

    IEnumerator GameOver()
    {
        Debug.Log( "GameOver begins" );

        _gateBlock.Stop();
        yield return new WaitForSeconds( 2.0f );
        
        // Display game over overlay
        _gameoverCanvas.SetActive( true );

        yield return WaitForPlayerInput();

       // hide game over 
        _gameoverCanvas.SetActive( false );

       Debug.Log( "GameOver ends" );
    }
    
#endregion

    void OnApplicationQuit()
    {
        instance = null;
    }


#region Handle Player Events

    public void OnExit()
    {
        if( _loadingLevel )
            return;

        _loadingLevel = true;
        Debug.Log( "[Game] OnExit" );

        Invoke( "LoadNextLevel" , kReloadDelay ); 
    }

    public void OnDeath()
    {
        Debug.Log( "[Game] OnDeath" );
            
        _isPlayerAlive = false;
        _background.enabled = false;
    }

#endregion

    IEnumerator ReloadLevel()
    {
        yield return new WaitForSeconds( kReloadDelay );
        Application.LoadLevel( Application.loadedLevel );
    }

    void LoadNextLevel()
    {
        if( reloadLevel )
            Application.LoadLevel( Application.loadedLevel );
        else
        {
            int nextLevel =  Application.loadedLevel + 1;
            
            if( nextLevel <= kLastLevel )
            {
                Application.LoadLevel( nextLevel );
            }
            else
            {
                LoadVictory();
            }
        }
    }

    void LoadVictory()
    {
        //menu.ShowVictory();
    }

    IEnumerator WaitForPlayerInput()
    {
        bool running = true;
        while( running )
        {
            if( Input.GetKeyDown( KeyCode.Space ) )
                running = false;

            yield return null;
        }
    }

    public void OnBlockTransport( GateBlock block )
    {
        block.LoadCombination( _blockConsumer.ConsumeBlock() );
    }
}
