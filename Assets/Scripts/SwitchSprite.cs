﻿using UnityEngine;
using System.Collections;

[RequireComponent( typeof( SpriteRenderer ) )]
public class SwitchSprite : MonoBehaviour 
{
    [SerializeField] Sprite[] _spriteArray;

    private SpriteRenderer _renderer;
    private int _currentId;

    void Awake()
    {
        _renderer = GetComponent<SpriteRenderer>();

        // load first sprite in the array
        _currentId = _spriteArray.Length;
        Switch();
    }

    public void Switch()
    {
        _currentId++;
        if( _currentId >= _spriteArray.Length )
            _currentId = 0;

        SetSpriteWithIndex( _currentId );
    }

    public void SetSpriteWithIndex( int index )
    {
        _currentId = index;
        _renderer.sprite = _spriteArray[ index ];
    }

    
}
