﻿using UnityEngine;
using System.Collections;

public class Transport : MonoBehaviour 
{
    [SerializeField] Transform _targetSpawnPoint;

    void OnTriggerEnter2D( Collider2D other )
    {
        if( other.CompareTag( "Block" ) )
        {
            other.transform.position = _targetSpawnPoint.position;

            Game.instance.OnBlockTransport( other.GetComponent<GateBlock>() );
        }
    }
}
