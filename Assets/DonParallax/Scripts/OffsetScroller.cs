﻿using UnityEngine;
using System.Collections;

public class OffsetScroller : MonoBehaviour 
{

	public float scrollSpeedX;
	public float scrollSpeedY;

	private Vector2 savedOffset;
    private float _timer;
	
	void Start () 
	{
		savedOffset = GetComponent<Renderer>().sharedMaterial.GetTextureOffset( "_MainTex" );
	}

	void Update () 
	{
        _timer += Time.deltaTime;

		float x = Mathf.Repeat( scrollSpeedX * _timer , 1 );	
		float y = Mathf.Repeat( scrollSpeedY * _timer , 1 );	
		//var offset = new Vector2( savedOffset.x + x , savedOffset.y + y );
		var offset = new Vector2( x , y );

		SetYOffsetOnMainTex( offset );

	}

	void OnDisable()
	{
		SetYOffsetOnMainTex( savedOffset );
	}

	void SetYOffsetOnMainTex( Vector2 offset )
	{
		GetComponent<Renderer>().sharedMaterial.SetTextureOffset( "_MainTex" , offset );
	}

    public void ResetOffset()
    {
        _timer = 0;
        SetYOffsetOnMainTex( Vector2.zero );
    }
}
